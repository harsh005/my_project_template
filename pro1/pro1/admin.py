from django.contrib import admin
from django.db import models
from pro1.models import *
class bba_admin(admin.ModelAdmin):
	list_display=['c_name','c_fee','c_rank']
	search_fields=['c_name','c_fee','c_rank']
class bca_admin(admin.ModelAdmin):
	list_display=['c_name','c_fee','c_rank']
	search_fields=['c_name','c_fee','c_rank']
class bcom_admin(admin.ModelAdmin):
	list_display=['c_name','c_fee','c_rank']
	search_fields=['c_name','c_fee','c_rank']
admin.site.register(Feedback)
admin.site.register(bca,bca_admin)
admin.site.register(bba,bba_admin)
admin.site.register(bcom,bcom_admin)