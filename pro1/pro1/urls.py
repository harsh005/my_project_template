from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pro1.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$','pro1.views.home',name='home'),
    url(r'^course/$','pro1.views.course',name='course'),
    url(r'^query$','pro1.views.query',name='query'),
    #url(r'^auth_login$','pro1.views.auth_login',name='home1'),
    url(r'^index/$','pro1.views.home',name='home'),
    url(r'^rank/$','pro1.views.rank',name='rank'),
    url(r'^rank1/$','pro1.views.rank1',name='rank1'),
    url(r'^rank2/$','pro1.views.rank2',name='rank2'),
    url(r'^sub/$','pro1.views.sub',name='sub'),
   )
